<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Command;
use app\models\Player;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Players';
$this->params['breadcrumbs'][] = $this->title;

$dataprovider = new ActiveDataProvider([
    'query' => Player::find()->where(['commandid' => $commandID]), 
    'pagination' => ['pageSize' => 10,],
]);
 ?>    

<?php
$command = new Command();
echo "<h2>"."Command is ".$command->GetCommandName($commandID)->CommandName."</h2>";
?>

<div align="right">
    <?= Html::a('Add player', ['/player/player'], ['class'=>'btn btn-primary']) ?>  
</div>

<?php
echo GridView::widget([
    'dataProvider' => $dataprovider,
    'columns' => [
        'id',
       'UserName',
       'UserLastName',
       'DateOfBirth',
       'Status',
       ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Action',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model)
                        {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['player/update', 'id' => $model->id]));
                        },
                        
                        'delete' => function ($url, $model) 
                        {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['player/delete', 'id' => $model->id]));
                        },
                    ],
                ],
    ],
]);

?>

