<?php

/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Command;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'My Yii Application';

$dataprovider = new ActiveDataProvider([
    'query' => Command::find(), 
    'pagination' => ['pageSize' => 10,],
]);

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Command Manager</h1>
        <p class="lead">You have successfully created your Yii-powered application.</p>
    </div>

    <div class="body-content">
   
    <div align="right">
        <?= Html::a('Add command', ['/site/command'], ['class'=>'btn btn-primary']) ?>  
    </div>
     
    <?php
        echo GridView::widget([
            'dataProvider' => $dataprovider,
                'columns' => [
                'Id',
                ['attribute' => 'CommandName',
                
                'value' => function(Command $model)
                {
                    return Html::a(Html::encode($model->CommandName), Url::to(['site/view', 'id' => $model->Id]));
                }
                ,'format' => 'raw',
                ],
                'YearCreate',
                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Action',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function($url, $model)
                        {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                        },
                        
                        'update' => function ($url, $model)
                        {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                        },
                        
                        'delete' => function ($url, $model) 
                        {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url);
                        },
                    ],
                
                'urlCreator' => function ($action, $model, $url, $key)
                {
                    if($action === 'view')
                    {
                        $url = Url::to(['site/view', 'id' => $model->Id]);
                    }
                    
                    if($action === 'update')
                    {
                        $url = Url::to(['site/update', 'id' => $model->Id]);
                    }
                    
                    if($action === 'delete')
                    {
                        $url = Url::to(['site/delete', 'id' => $model->Id]);
                    }
                    return $url;
                }
                ],
            ],
        ]);
        ?>
        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
