<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\jui\DatePicker;
?>
<h3>Command form</h3>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'CommandName')->textInput(['placeholder' => 'Command', 'style'=>'width:300px'])?>
<?= $form->field($model, 'YearCreate')->widget(DatePicker::classname(), 
    ['options' => ['placeholder' => 'Choise year', 'class' => 'form-control', 'style'=>'width:300px' ],'dateFormat' => 'yyyy'],
    
    [
        'language' => 'en',
])?>
<?= Html::submitButton('Submit', ['class' => 'btn btn-primary'])?>
<?php ActiveForm::end() ?>