<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\jui\DatePicker;
    use yii\helpers\ArrayHelper;
    use app\models\Command;
?>
<h3>Player form</h3>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'UserName')->textInput(['placeholder' => 'Name', 'style'=>'width:300px'])?>
<?= $form->field($model, 'UserLastName')->textInput(['placeholder' => 'LastName', 'style'=>'width:300px'])?>

<?= $form->field($model, 'DateOfBirth')->widget(DatePicker::classname(), 
    ['options' => ['placeholder' => 'Date', 'class' => 'form-control', 'style'=>'width:300px' ],'dateFormat' => 'yyyy-MM-dd', ],
    
    [
        'language' => 'en',
])?>

<?= $form->field($model, 'Status')->dropDownList([ 1 => 'forward', 2 => 'goalkeeper', 3 => 'half-back', 4 => 'defender'],['style'=>'width:300px'])?>
<?= $form->field($model, 'CommandId')->dropDownList(ArrayHelper::map(Command::find()->all(), 'Id','CommandName'),['placeholder' => 'command', 'style'=>'width:300px'])?>
<?= Html::submitButton('Submit', ['class' => 'btn btn-primary'])?>
<?php ActiveForm::end() ?>
