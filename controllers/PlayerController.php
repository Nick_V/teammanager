<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\PlayerForm;
use app\models\Player;


class PlayerController extends Controller
{
    public function actionPlayer()
    {
        $model = new PlayerForm();
        if($model->load(Yii::$app->request->post()) && $model->validate())
        {   
            $newmodel = new Player();
            $newmodel->UserName = $model->UserName;
            $newmodel->UserLastName = $model->UserLastName;
            $newmodel->DateOfBirth = $model->DateOfBirth;
            $newmodel->Status = $model->Status;
            $newmodel->CommandId = $model->CommandId;
            $newmodel->save();
            return $this->redirect(['site/view', 'id' => $model->CommandId]);
        }
        return $this->render('player',['model' => $model]);
    }
    
    public function actionUpdate($id)
    {
        $model = new Player();
        $newmodel = Player::findOne(['Id' => $id]);
        
        if($model->load(Yii::$app->request->post())&& $model->validate())
        {
            $newmodel->UserName = $model->UserName;
            $newmodel->UserLastName = $model->UserLastName;
            $newmodel->DateOfBirth = $model->DateOfBirth;
            $newmodel->Status = $model->Status;
            $newmodel->CommandId = $model->CommandId;
            $newmodel->save();
            return $this->redirect(['site/view', 'id' => $model->CommandId]);
        }
        return $this->render('player',['model' => $newmodel]);
    }
    
    public function actionDelete($id)
    {
        $model =  Player::findOne(['Id' => $id]);
        $model->delete();
        return $this->redirect(['site/view', 'id' => $model->CommandId]);
    }
    
}

