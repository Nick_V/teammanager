<?php

namespace app\models;

class Player extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'players';        
    }
    
    public function rules() {
        return[
            [['UserName', 'UserLastName', 'DateOfBirth', 'Status', 'CommandId'], 'required'],
            ['UserName', 'string', 'max' => 255 ],
            ['UserLastName', 'string', 'max' => 255 ],
            
        ];
    }
}

