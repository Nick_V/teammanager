<?php

namespace app\models;
use yii\base\Model;

class CommandForm extends Model
{
    public $CommandName;
    public $YearCreate;
    
    public function rules() {
        return[
            [['CommandName', 'YearCreate' ], 'required' ],
            ['CommandName', 'string', 'max' => 255 ],
            ['CommandName', 'unique',
                'targetClass' => Command::className(),
                'message' => 'This team is already registered!'
                
                ]
        ];
    }
}

