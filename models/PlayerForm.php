<?php

namespace app\models;
use yii\base\Model;

class PlayerForm extends Model
{
    public $UserName;
    public $UserLastName;
    public $DateOfBirth;
    public $Status;
    public $CommandId;
    
     public function rules() {
        return[
            [['UserName', 'UserLastName', 'DateOfBirth', 'Status', 'CommandId'], 'required'],
            ['UserName', 'string', 'max' => 255 ],
            ['UserLastName', 'string', 'max' => 255 ],
            
        ];
    }
}