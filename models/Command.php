<?php

namespace app\models;

class Command extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'commands';        
    }
    
    public function rules() {
        return[
            [['CommandName', 'YearCreate' ], 'required' ],
            ['CommandName', 'string', 'max' => 255 ],
            ['CommandName', 'unique',
                'targetClass' => Command::className(),
                'message' => 'This team is already registered!'
                
                ]
        ];
    }
   
    public function GetCommandName($id)
    {
        return Command::findOne(['Id' => $id]);
    }
}


